/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/JnettopConnectionWidget.java,v 1.7 2006-04-30 12:52:48 merunka Exp $
 *
 */

package info.kubs.jnettop.gui;

import info.kubs.jnettop.client.JnettopInterface;
import info.kubs.jnettop.client.JnettopSession;
import info.kubs.jnettop.client.JnettopSessionListener;
import info.kubs.jnettop.client.JnettopStream;

import java.text.Collator;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

public class JnettopConnectionWidget extends Composite implements JnettopSessionListener {

	public JnettopConnectionWidget(Composite parent, int style) {
		super(parent, style);
		constructControls();
	}
	
	private Composite	topBarComposite;
	private Composite	bottomComposite;
	
	private Combo			interfaceCombo;
	private Button			runButton;
	private Button			stopButton;
	private Button			columnsButton;
	private TableViewer		streamsTableViewer;
	
	private JnettopConnectionData	connectionData;
	private JnettopConnection		connection;
	private JnettopSession			session;
	
	public static final int COLUMN_LOCAL_IP = 0;
	public static final int COLUMN_LOCAL_NAME = 1;
	public static final int COLUMN_LOCAL_PORT = 2;
	public static final int COLUMN_LOCAL_BYTES = 3;
	public static final int COLUMN_LOCAL_PACKETS = 4;
	public static final int COLUMN_LOCAL_BPS = 5;
	public static final int COLUMN_LOCAL_PPS = 6;
	public static final int COLUMN_REMOTE_IP = 7;
	public static final int COLUMN_REMOTE_NAME = 8;
	public static final int COLUMN_REMOTE_PORT = 9;
	public static final int COLUMN_REMOTE_BYTES = 10;
	public static final int COLUMN_REMOTE_PACKETS = 11;
	public static final int COLUMN_REMOTE_BPS = 12;
	public static final int COLUMN_REMOTE_PPS = 13;
	public static final int COLUMN_TOTAL_BYTES = 14;
	public static final int COLUMN_TOTAL_PACKETS = 15;
	public static final int COLUMN_TOTAL_BPS = 16;
	public static final int COLUMN_TOTAL_PPS = 17;
	public static final int COLUMN_PROTOCOL = 18;
	public static final int COLUMN_FILTER_DATA = 19;
	
	private ColumnsConfigurationToolbar columnsConfigurationToolbar;
	
	private void constructControls() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		gridLayout.marginTop = 5;
		this.setLayout(gridLayout);
		
		this.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				onDispose();
			}
		});
		
		GridLayout topBarLayout = new GridLayout(5, false);
		topBarComposite = new Composite(this, 0);
		topBarComposite.setLayout(topBarLayout);
		topBarComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		
		bottomComposite = new Composite(this, 0);
		bottomComposite.setLayout(new FillLayout());
		bottomComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		Label lbl = new Label(topBarComposite, 0);
		lbl.setText("Interface:");
		lbl.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		
		interfaceCombo = new Combo(topBarComposite, SWT.DROP_DOWN | SWT.READ_ONLY);
		interfaceCombo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		
		runButton = new Button(topBarComposite, 0);
		runButton.setText("Run");
		runButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		runButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				runButtonPressed();
			}
		});
		
		stopButton = new Button(topBarComposite, 0);
		stopButton.setText("Stop");
		stopButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		stopButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				stopButtonPressed();
			}
		});
		stopButton.setEnabled(false);
		
		columnsButton = new Button(topBarComposite, 0);
		columnsButton.setText("Configure Columns");
		columnsButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
		columnsButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				columnsButtonPressed();
			}
		});

		Table streamsTable = new Table(bottomComposite, SWT.FULL_SELECTION);
		streamsTableViewer = new TableViewer(streamsTable);
		streamsTableViewer.setContentProvider(new JnettopTableContentProvider());
		streamsTableViewer.setLabelProvider(new JnettopTableLabelProvider());
		streamsTable.setHeaderVisible(true);
		streamsTable.setLinesVisible(true);

		columnsConfigurationToolbar = new ColumnsConfigurationToolbar(this.getShell(), streamsTableViewer.getTable(), "maincolumns");
		
		createColumn("Local IP", 100, SWT.LEFT);
		createColumn("Local Name", 100, SWT.LEFT);
		createColumn("Local Port", 100, SWT.RIGHT);
		createColumn("Local Bytes", 100, SWT.RIGHT);
		createColumn("Local Packets", 100, SWT.RIGHT);
		createColumn("Local BPS", 100, SWT.RIGHT);
		createColumn("Local PPS", 100, SWT.RIGHT);
		createColumn("Remote IP", 100, SWT.LEFT);
		createColumn("Remote Name", 100, SWT.LEFT);
		createColumn("Remote Port", 100, SWT.RIGHT);
		createColumn("Remote Bytes", 100, SWT.RIGHT);
		createColumn("Remote Packets", 100, SWT.RIGHT);
		createColumn("Remote BPS", 100, SWT.RIGHT);
		createColumn("Remote PPS", 100, SWT.RIGHT);
		createColumn("Total Bytes", 100, SWT.RIGHT);
		createColumn("Total Packets", 100, SWT.RIGHT);
		createColumn("Total BPS", 100, SWT.RIGHT);
		createColumn("Total PPS", 100, SWT.RIGHT);
		createColumn("Protocol", 100, SWT.LEFT);
		createColumn("Filter Data", 100, SWT.LEFT);
		
		columnsConfigurationToolbar.create();
	}
	
	private TableColumn createColumn(String name, int width, int alignment) {
		Table table = streamsTableViewer.getTable();
		
		final int columnIndex = table.getColumnCount();
		
		TableColumn column = new TableColumn(table, 0);
		column.setText(name);
		column.setWidth(width);
		column.setAlignment(alignment);
		column.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				streamsTableViewer.setSorter(new JnettopTableSorter(columnIndex));
			}
		});
		
		columnsConfigurationToolbar.setColumnData(column, new ColumnsConfigurationToolbar.ColumnData(width));
		
		return column;
	}

	private void runButtonPressed() {
		if (interfaceCombo.getText().length() == 0) {
			return;
		}
		try {
			session.selectInterface(interfaceCombo.getText());
			session.run();
			runButton.setEnabled(false);
			stopButton.setEnabled(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void stopButtonPressed() {
		try {
			session.stop();
			runButton.setEnabled(true);
			stopButton.setEnabled(false);
		} catch (Exception e) {
			
		}
	}
	
	private void columnsButtonPressed() {
		columnsConfigurationToolbar.open();
	}

	public void dataAvailable(final JnettopSession session) {
		this.getDisplay().asyncExec(new Runnable() {
			public void run() {
				updateFromSession(session);
			}
		});
	}

	private void updateFromSession(JnettopSession session) {
		if (!isDisposed()) {
			streamsTableViewer.setInput(session);
		}
	}

	private void onDispose() {
		if (this.connection != null) {			
			session.setSessionListener(null);
			session.close();
			connection.close();
		}
	}
	
	public void setConnectionAndData(JnettopConnectionData connectionData, JnettopConnection connection) {
		if (this.connection != null) {
			throw new IllegalStateException("Do not set Jnettop connection twice!");
		}
		this.connectionData = connectionData;
		this.connection = connection;
		session = connection.getSession();
		session.setSessionListener(this);
		for (JnettopInterface intf : session.getInterfaces()) {
			interfaceCombo.add(intf.getInterfaceName());
		}
		columnsConfigurationToolbar.restoreSettings(this.connectionData.getColumnsConfiguration());
	}
	
	public JnettopConnectionData getConnectionData() {
		connectionData.setColumnsConfiguration(columnsConfigurationToolbar.saveSettings());
		return connectionData;
	}
	
	public JnettopConnection getConnection() {
		return connection;
	}

	private static class JnettopTableContentProvider implements IStructuredContentProvider {
		public Object[] getElements(Object inputElement) {
			JnettopSession session = (JnettopSession) inputElement;
			HashMap<Long, JnettopStream> streams = session.getStreams();
			JnettopStream[] streamsArray = new JnettopStream[streams.size()];
			streams.values().toArray(streamsArray);
			return streamsArray;
		}

		public void dispose() {
		}

		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
	
	private static class JnettopTableLabelProvider implements ITableLabelProvider {
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}
		
		private String toBytesString(long number) {
			return Long.toString(number);			
		}
		
		private String toPacketsString(long number) {
			return Long.toString(number);
		}
		
		private String toBpsString(long number) {
			return Long.toString(number);
		}
		
		private String toPpsString(long number) {
			return Long.toString(number);
		}

		public String getColumnText(Object element, int columnIndex) {
			JnettopStream stream = (JnettopStream) element;
			switch (columnIndex) {
			case COLUMN_LOCAL_IP: return stream.localAddress;
			case COLUMN_LOCAL_NAME: return stream.getLocalName();
			case COLUMN_LOCAL_PORT: return Integer.toString(stream.localPort);
			case COLUMN_LOCAL_BYTES: return toBytesString(stream.localBytes);
			case COLUMN_LOCAL_PACKETS: return toPacketsString(stream.localPackets);
			case COLUMN_LOCAL_BPS: return toBpsString(stream.localBps);
			case COLUMN_LOCAL_PPS: return toPpsString(stream.localPps);
			case COLUMN_REMOTE_IP: return stream.remoteAddress;
			case COLUMN_REMOTE_NAME: return stream.getRemoteName();
			case COLUMN_REMOTE_PORT: return Integer.toString(stream.remotePort);
			case COLUMN_REMOTE_BYTES: return toBytesString(stream.remoteBytes);
			case COLUMN_REMOTE_PACKETS: return toPacketsString(stream.remotePackets);
			case COLUMN_REMOTE_BPS: return toBpsString(stream.remoteBps);
			case COLUMN_REMOTE_PPS: return toPpsString(stream.remotePps);
			case COLUMN_TOTAL_BYTES: return toBytesString(stream.totalBytes);
			case COLUMN_TOTAL_PACKETS: return toPacketsString(stream.totalPackets);
			case COLUMN_TOTAL_BPS: return toBpsString(stream.totalBps);
			case COLUMN_TOTAL_PPS: return toPpsString(stream.totalPps);
			case COLUMN_PROTOCOL: return stream.protocol;
			case COLUMN_FILTER_DATA: return stream.filterData;
			}
			return Long.toString(stream.uid, 16);
		}

		public void addListener(ILabelProviderListener listener) {
		}

		public void dispose() {
		}

		public boolean isLabelProperty(Object element, String property) {
			return false;
		}

		public void removeListener(ILabelProviderListener listener) {
		}
	}
	
	private static class JnettopTableSorter extends ViewerSorter implements Comparator<Object> {
		
		private int columnIndex;
		
		public JnettopTableSorter(int columnIndex) {
			this.columnIndex = columnIndex;
		}

		@Override
		public int category(Object element) {
			return 0;
		}
		
		private int signum(long number) {
			if (number == 0)
				return 0;
			if (number < 0)
				return -1;
			return 1;
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			return compare(e1, e2);
		}

		@Override
		public Collator getCollator() {
			return super.getCollator();
		}

		@Override
		public boolean isSorterProperty(Object element, String property) {
			return false;
		}

		@Override
		public void sort(Viewer viewer, Object[] elements) {
			Arrays.sort(elements, this);
		}

		public int compare(Object e1, Object e2) {
			JnettopStream s1 = (JnettopStream) e1;
			JnettopStream s2 = (JnettopStream) e2;
			
			switch (columnIndex) {
			case COLUMN_LOCAL_IP: return s1.localAddress.compareTo(s2.localAddress);
			case COLUMN_LOCAL_NAME: return s1.getLocalName().compareTo(s2.getLocalName());
			case COLUMN_LOCAL_PORT: return s2.localPort - s1.localPort;
			case COLUMN_LOCAL_BYTES: return signum(s2.localBytes - s1.localBytes);
			case COLUMN_LOCAL_PACKETS: return signum(s2.localPackets - s1.localPackets);
			case COLUMN_LOCAL_BPS: return signum(s2.localBps - s1.localBps);
			case COLUMN_LOCAL_PPS: return signum(s2.localPps - s1.localPps);
			case COLUMN_REMOTE_IP: return s1.remoteAddress.compareTo(s2.remoteAddress);
			case COLUMN_REMOTE_NAME: return s1.getRemoteName().compareTo(s2.getRemoteName());
			case COLUMN_REMOTE_PORT: return s2.remotePort - s1.remotePort;
			case COLUMN_REMOTE_BYTES: return signum(s2.remoteBytes - s1.remoteBytes);
			case COLUMN_REMOTE_PACKETS: return signum(s2.remotePackets - s1.remotePackets);
			case COLUMN_REMOTE_BPS: return signum(s2.remoteBps - s1.remoteBps);
			case COLUMN_REMOTE_PPS: return signum(s2.remotePps - s1.remotePps);
			case COLUMN_TOTAL_BYTES: return signum(s2.totalBytes - s1.totalBytes);
			case COLUMN_TOTAL_PACKETS: return signum(s2.totalPackets - s1.totalPackets);
			case COLUMN_TOTAL_BPS: return signum(s2.totalBps - s1.totalBps);
			case COLUMN_TOTAL_PPS: return signum(s2.totalPps - s1.totalPps);
			case COLUMN_PROTOCOL: return s1.protocol.compareTo(s2.protocol);
			case COLUMN_FILTER_DATA: return s1.filterData.compareTo(s2.filterData);
			}
			
			return 0;
		}
	}
}
