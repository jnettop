/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/local/JnettopLocalConnectionData.java,v 1.1 2006-04-30 12:52:25 merunka Exp $
 *
 */

package info.kubs.jnettop.gui.local;

import info.kubs.jnettop.gui.JnettopConnectionData;
import info.kubs.jnettop.gui.JnettopConnectionFactory;

import java.util.prefs.Preferences;

public class JnettopLocalConnectionData extends JnettopConnectionData {
	
	private String command;

	public JnettopLocalConnectionData(JnettopConnectionFactory factory, String prefix, Preferences prefs) {
		super(factory, prefix, prefs);
		this.command = prefs.get(prefix + ".command", "sudo /usr/bin/jnettop --display jnet");
	}

	public JnettopLocalConnectionData(JnettopConnectionFactory factory, String command) {
		super(factory, "Local exec");
		this.command = command;
	}
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	@Override
	public void store(String prefix, Preferences prefs) {
		super.store(prefix, prefs);
		prefs.put(prefix + ".command", command);
	}	
}
