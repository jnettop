/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/local/JnettopLocalConnection.java,v 1.1 2006-04-30 12:52:25 merunka Exp $
 *
 */

package info.kubs.jnettop.gui.local;

import info.kubs.jnettop.client.JnettopSession;
import info.kubs.jnettop.client.JnettopSessionSite;
import info.kubs.jnettop.gui.JnettopConnection;
import info.kubs.jnettop.gui.JnettopConnectionData;
import info.kubs.jnettop.gui.JnettopConnectionFactory;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

public class JnettopLocalConnection implements JnettopConnection {

	private JnettopLocalConnectionFactory	factory;
	private JnettopLocalConnectionData		connectionData;
	
	private Process			runningProcess;
	private Shell			parentShell;
	private JnettopSession	jnSession;
	
	public JnettopLocalConnection(JnettopLocalConnectionFactory factory, Shell parentShell) {
		this.parentShell = parentShell;
		this.factory = factory;
	}

	public void setConnectionData(JnettopLocalConnectionData data) {
		this.connectionData = data;
	}

	public JnettopSession connect() throws Exception {
		runningProcess = Runtime.getRuntime().exec(connectionData.getCommand());
		
		jnSession = new JnettopSession(runningProcess.getInputStream(), runningProcess.getOutputStream());
		jnSession.setSessionSite(new SessionSite());

		jnSession.initialize();
		
		return jnSession;
	}

	public void close() {
		try {
			runningProcess.destroy();
		} catch (final Exception e) {
			parentShell.getDisplay().syncExec(new Runnable() {
				public void run() {
					MessageDialog.openInformation(parentShell, "Jnettop Local exec session destroy exception", e.toString());
				}
			});
		}
	}

	public JnettopSession getSession() {
		return jnSession;
	}
	
	public JnettopConnectionData getConnectionData() {
		return connectionData;
	}

	public JnettopConnectionFactory getFactory() {
		return factory;
	}

	private class SessionSite implements JnettopSessionSite {
		public void showMessage(final String message) {
			parentShell.getDisplay().syncExec(new Runnable () {
				public void run() {
					MessageDialog.openInformation(parentShell, "Jnettop Session Information", message);
				}
			});		
		}
	}
}
