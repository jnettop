/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/local/JnettopEditLocalConnectionControl.java,v 1.1 2006-04-30 12:52:25 merunka Exp $
 *
 */

package info.kubs.jnettop.gui.local;


import info.kubs.jnettop.gui.JnettopConnectionData;
import info.kubs.jnettop.gui.JnettopEditConnectionControl;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class JnettopEditLocalConnectionControl extends JnettopEditConnectionControl {

	private JnettopLocalConnectionFactory factory;
	private Text commandText;

	public JnettopEditLocalConnectionControl(Composite parent, int style, JnettopLocalConnectionFactory factory) {
		super(parent, style);
		this.factory = factory;
		createControls();
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	private void createControls() {
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		this.setLayout(gridLayout);
		
		final Label commandLabel = new Label(this, SWT.NONE);
		commandLabel.setText("Command:");

		commandText = new Text(this, SWT.BORDER);
		commandText.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		commandText.setText("sudo /usr/bin/jnettop --display jnet");
	}
	
	@Override
	public String validateAndGetError() {
		if (commandText.getText().length() == 0) {
			return "You have to specify command to run.";
		}
		return null;
	}
	
	@Override
	public String getIntro() {
		return "Local exec invokes jnettop locally (possibly via sudo) and is the way to go with the least resources";
	}

	public JnettopConnectionData getConnectionData() {
		return new JnettopLocalConnectionData(factory, commandText.getText());
	}
}
