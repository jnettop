/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/JnettopGui.java,v 1.9 2006-05-01 13:20:01 merunka Exp $
 *
 */

package info.kubs.jnettop.gui;

import info.kubs.jnettop.gui.local.JnettopLocalConnectionFactory;
import info.kubs.jnettop.gui.ssh.JnettopSshConnectionFactory;

import java.util.ArrayList;
import java.util.prefs.Preferences;

import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

import com.swtdesigner.SWTResourceManager;

public class JnettopGui extends ApplicationWindow {
	
	public static final String VERSION_NUMBER = "0.1";
	
	private static JnettopGui instance;
	public static JnettopGui getInstance() { return instance; }
	
	private static ArrayList<JnettopConnectionFactory> jnettopConnectionFactoryRegistry;
	public static ArrayList<JnettopConnectionFactory> getJnettopConnectionFactoryRegistry() {
		return jnettopConnectionFactoryRegistry;
	}
	
	public JnettopGui() {
		super(null);
	}
	
	private JnettopConnectionFactory findFactory(String name) {
		for (int i=0; i<jnettopConnectionFactoryRegistry.size(); i++) {
			if (jnettopConnectionFactoryRegistry.get(i).getName().equals(name)) {
				return jnettopConnectionFactoryRegistry.get(i);
			}
		}
		return null;
	}
	
	private ArrayList<JnettopConnectionData> connectionData;
	private void loadConnectionData() {
		try {
			int connectionCount = Integer.parseInt(getPreference("connectionDataCount", "0"));
			
			connectionData = new ArrayList<JnettopConnectionData>();
			for (int i=0; i<connectionCount; i++) {
				String factoryName = getPreference("connectionData."+i+".factoryName", null);
				JnettopConnectionFactory factory = findFactory(factoryName);
				if (factory == null)
					continue;

				JnettopConnectionData data = factory.createConnectionData("connectionData."+i, preferences);
				connectionData.add(data);
			}
		} catch (Exception e) {
			connectionData = new ArrayList<JnettopConnectionData>();
		}
	}
	
	private void saveConnectionData() {
		setPreference("connectionDataCount", Integer.toString(connectionData.size()));
		for (int i=0; i<connectionData.size(); i++) {
			JnettopConnectionData cd = connectionData.get(i);
			setPreference("connectionData."+i+".factoryName", cd.getFactory().getName());
			cd.store("connectionData."+i, preferences);
		}
	}
	
	public ArrayList<JnettopConnectionData> getConnectionData() {
		return connectionData;
	}
	
	private void loadPreferences() {
		preferences = Preferences.userNodeForPackage(this.getClass());
		loadConnectionData();
	}
	
	public void savePreferences() {
		saveConnectionData();
	}
	
	public String getPreference(String preferenceName, String defaultValue) {
		return preferences.get(preferenceName, defaultValue);
	}
	
	public void setPreference(String preferenceName, String value) {
		preferences.put(preferenceName, value);
	}
	
	public void removePreference(String preferenceName) {
		preferences.remove(preferenceName);
	}
	
	private void run() {
		instance = this;		
		jnettopConnectionFactoryRegistry = new ArrayList<JnettopConnectionFactory>();
		jnettopConnectionFactoryRegistry.add(new JnettopSshConnectionFactory());
		jnettopConnectionFactoryRegistry.add(new JnettopLocalConnectionFactory());
		setBlockOnOpen(true);
		loadPreferences();
		open();
		Display.getCurrent().dispose();
	}
	
	private Menu menuBar;
	
	private MenuItem connectionMenuItem;
	private Menu connectionMenu;
	private MenuItem newConnectionMenuItem;
	private MenuItem openConnectionMenuItem;
	private MenuItem saveConnectionMenuItem;
	private MenuItem saveConnectionAsMenuItem;
	private MenuItem exitMenuItem;
	
	private MenuItem helpMenuItem;
	private Menu helpMenu;
	private MenuItem aboutMenuItem;
	
	private JnettopMainWindowCompositeControl display;
	
	private Preferences preferences;
	
	@Override
	protected Control createContents(Composite parent) {
		menuBar = new Menu(this.getShell(), SWT.BAR);
		
		connectionMenuItem = new MenuItem(menuBar, SWT.CASCADE);
		connectionMenuItem.setText("&Connection");
		connectionMenu = new Menu(this.getShell(), SWT.DROP_DOWN);
		connectionMenuItem.setMenu(connectionMenu);
		
		newConnectionMenuItem = new MenuItem(connectionMenu, SWT.PUSH);
		newConnectionMenuItem.setText("&New Connection...\tCtrl+N");
		newConnectionMenuItem.setAccelerator(SWT.CONTROL | 'n');
		newConnectionMenuItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				invokeNewConnectionDialog();
			}
		});
		
		openConnectionMenuItem = new MenuItem(connectionMenu, SWT.PUSH);
		openConnectionMenuItem.setText("&Open Connection...\tCtrl+O");
		openConnectionMenuItem.setAccelerator(SWT.CONTROL | 'o');
		openConnectionMenuItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				invokeOpenConnectionDialog();
			}
		});
		
		saveConnectionMenuItem = new MenuItem(connectionMenu, SWT.PUSH);
		saveConnectionMenuItem.setText("&Save Connection\tCtrl+S");
		saveConnectionMenuItem.setAccelerator(SWT.CONTROL | 's');
		saveConnectionMenuItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				invokeSaveConnection();
			}
		});
		
		saveConnectionAsMenuItem = new MenuItem(connectionMenu, SWT.PUSH);
		saveConnectionAsMenuItem.setText("Save Connection &As...");
		saveConnectionAsMenuItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				invokeSaveConnectionAs();
			}
		});
		
		new MenuItem(connectionMenu, SWT.SEPARATOR);
		
		exitMenuItem = new MenuItem(connectionMenu, SWT.PUSH);
		exitMenuItem.setText("Exit");
		exitMenuItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				invokeExit();
			}
		});
		
		helpMenuItem = new MenuItem(menuBar, SWT.CASCADE);
		helpMenuItem.setText("&Help");
		helpMenu = new Menu(this.getShell(), SWT.DROP_DOWN);
		helpMenuItem.setMenu(helpMenu);
		
		aboutMenuItem = new MenuItem(helpMenu, SWT.PUSH);
		aboutMenuItem.setText("&About JnettopGui...");
		aboutMenuItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				invokeAboutDialog();
			}
		});
		
		this.getShell().setMenuBar(menuBar);
		
		new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL).setVisible(false);
		
		display = new JnettopMainWindowCompositeControl(parent, 0);
		return display;
	}

	public static void main(String[] args) {
		JnettopGui application = new JnettopGui();
		application.run();
		System.exit(0);
	}

	public void invokeNewConnectionDialog() {
		JnettopNewConnectionDialog dialog = new JnettopNewConnectionDialog(this.getShell());
		if (dialog.open() != SWT.OK)
			return;
		JnettopConnectionData connectionData = dialog.getCreatedConnectionData();
		runNewConnection(connectionData);
	}
	
	public void invokeOpenConnectionDialog() {
		JnettopOpenConnectionDialog dialog = new JnettopOpenConnectionDialog(this.getShell());
		if (dialog.open() != SWT.OK)
			return;
		JnettopConnectionData connectionData = dialog.getCreatedConnectionData();
		runNewConnection(connectionData);
	}
	
	public void invokeSaveConnection() {
		JnettopConnectionData connectionData = display.getSelectedConnectionData();
		if (connectionData == null)
			return;
		
		for (int i=0; i<this.connectionData.size(); i++) {
			if (this.connectionData.get(i).getDisplayName() == connectionData.getDisplayName()) {
				this.connectionData.set(i, (JnettopConnectionData) connectionData.clone());
				return;
			}
		}
		
		invokeSaveConnectionAs();
	}
	
	public void invokeSaveConnectionAs() {
		JnettopConnectionData connectionData = display.getSelectedConnectionData();
		if (connectionData == null)
			return;
		
		InputDialog dlg = new InputDialog(getShell(), "Save Connection As...", "Enter connection name:", connectionData.getDisplayName(), new IInputValidator() {
			public String isValid(String newText) {
				return newText.length()>0 ? null : "You have to supply connection name.";
			}
		});
	    if (dlg.open() == Window.OK) {
	    	connectionData.setDisplayName(dlg.getValue());
	    	this.connectionData.add((JnettopConnectionData) connectionData.clone());
	    }
	    
	    saveConnectionData();
	}
	
	public void runNewConnection(JnettopConnectionData connectionData) {
		try {
			JnettopConnection connection = connectionData.getFactory().createConnection(getShell(), connectionData);
			connection.connect();
			display.addConnectionTab(connectionData, connection);
		} catch (Exception e) {
			MessageDialog.openError(this.getShell(), "Connection error:", e.toString());
		}
	}
	
	public void invokeAboutDialog() {
		AboutDialog dlg = new AboutDialog(getShell());
		dlg.open();
	}
	
	public void invokeExit() {
		close();
	}

	@Override
	protected boolean showTopSeperator() {
		return false;
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Jnettop Gui");
		shell.setImage(SWTResourceManager.getImage(JnettopGui.class, "jnettop.png"));
		Window.setDefaultImage(SWTResourceManager.getImage(JnettopGui.class, "jnettop.png"));
	}
}
