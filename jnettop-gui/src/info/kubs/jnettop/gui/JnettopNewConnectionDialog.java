/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/JnettopNewConnectionDialog.java,v 1.1 2006-04-30 12:52:49 merunka Exp $
 *
 */

package info.kubs.jnettop.gui;


import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.swtdesigner.SWTResourceManager;

public class JnettopNewConnectionDialog extends Dialog {
	
	private Composite					mainComposite;
	private Combo						connectionFactoryCombo;
	private CLabel						validationLabel;						
	private JnettopEditConnectionControl	connectionFactoryControls[];
	private JnettopEditConnectionControl	currentConnectionControl;
	
	private JnettopConnectionData		createdConnectionData;

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		getShell().setText("Create new connection...");
		
		GridLayout gridLayout = new GridLayout(2, false);
		mainComposite = new Composite(parent, 0);
		mainComposite.setLayout(gridLayout);
		
		Label lbl = new Label(mainComposite, 0);
		lbl.setText("Connection type:");
		lbl.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		
		connectionFactoryCombo = new Combo(mainComposite, SWT.DROP_DOWN | SWT.READ_ONLY);
		connectionFactoryCombo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
		connectionFactoryCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				changeActiveConnectionFactory(connectionFactoryCombo.getSelectionIndex());
			}
		});
		
		lbl = new Label(mainComposite, SWT.SEPARATOR | SWT.HORIZONTAL);
		GridData gd = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd.verticalIndent = 3;
		lbl.setLayoutData(gd);
		
		validationLabel = new CLabel(mainComposite, 0);
		gd = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd.verticalIndent = 3;
		validationLabel.setLayoutData(gd);
		
		connectionFactoryControls = new JnettopEditConnectionControl[JnettopGui.getJnettopConnectionFactoryRegistry().size()];
		for (int i=0; i<connectionFactoryControls.length; i++) {
			JnettopConnectionFactory connectionFactory = JnettopGui.getJnettopConnectionFactoryRegistry().get(i);
			connectionFactoryControls[i] = connectionFactory.createEditConnectionControl(mainComposite);
			gd = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
			gd.verticalIndent = 3;
			connectionFactoryControls[i].setLayoutData(gd);
			connectionFactoryCombo.add(connectionFactory.getName());
		}
		connectionFactoryCombo.select(0);
		changeActiveConnectionFactory(0);

		return mainComposite;
	}
	
	private void changeActiveConnectionFactory(int index) {
		for (int i=0; i<connectionFactoryControls.length; i++) {
			GridData gd = (GridData) connectionFactoryControls[i].getLayoutData();
			gd.exclude = i != index;
			connectionFactoryControls[i].setVisible(i == index);
		}
		currentConnectionControl = connectionFactoryControls[index];
		validationLabel.setImage(SWTResourceManager.getImage(JnettopGui.class, "infoicon.png"));
		validationLabel.setText(currentConnectionControl.getIntro());
		mainComposite.layout();
	}

	public JnettopNewConnectionDialog(Shell parentShell) {
		super(parentShell);
		setBlockOnOpen(true);
	}

	@Override
	protected void cancelPressed() {
		this.setReturnCode(SWT.CANCEL);
		this.close();
	}

	@Override
	protected void okPressed() {
		String validationError = currentConnectionControl.validateAndGetError();
		if (validationError != null) {
			validationLabel.setImage(SWTResourceManager.getImage(JnettopGui.class, "erroricon.png"));
			validationLabel.setText(validationError);
			return;
		}
		createdConnectionData = currentConnectionControl.getConnectionData();
		this.setReturnCode(SWT.OK);
		this.close();
	}
	
	public JnettopConnectionData getCreatedConnectionData() {
		return createdConnectionData;
	}
}
