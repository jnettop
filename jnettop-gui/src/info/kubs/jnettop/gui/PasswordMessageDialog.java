/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/PasswordMessageDialog.java,v 1.2 2006-04-30 12:52:49 merunka Exp $
 *
 */

package info.kubs.jnettop.gui;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class PasswordMessageDialog extends MessageDialog {
	
	private String text;
	private Text textField;
	
	protected PasswordMessageDialog(Shell parentShell, String dialogTitle, String dialogMessage) {
		super(parentShell, 
				dialogTitle, 
				null, 
				dialogMessage, 
				MessageDialog.NONE, 
				new String[] { IDialogConstants.OK_LABEL, IDialogConstants.CANCEL_LABEL }, 
				0);
	}

	@Override
	protected Control createCustomArea(Composite parent) {
		textField = new Text(parent, SWT.BORDER);
		textField.setEchoChar('*');
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		textField.setLayoutData(data);
		return textField;
	}
	
	@Override
	protected void buttonPressed(int buttonId) {
		text = textField.getText();
		super.buttonPressed(buttonId);
	}

	public static String openPasswordQuery(Shell parentShell, String dialogTitle, String dialogMessage, String oldPassword) {
		boolean okPressed;
		
		PasswordMessageDialog dialog = new PasswordMessageDialog(parentShell, dialogTitle, dialogMessage);
		dialog.create();
		if (oldPassword != null) {
			dialog.textField.setText(oldPassword);
		}
		okPressed = dialog.open() == 0;
		return okPressed ? dialog.text : null;
	}

	public static String openPasswordQuery(Shell parentShell, String dialogTitle, String dialogMessage) {
		return openPasswordQuery(parentShell, dialogTitle, dialogMessage, null);
	}
}
