/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/ssh/JnettopEditSshConnectionControl.java,v 1.1 2006-04-30 12:52:25 merunka Exp $
 *
 */

package info.kubs.jnettop.gui.ssh;


import info.kubs.jnettop.gui.JnettopConnectionData;
import info.kubs.jnettop.gui.JnettopEditConnectionControl;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class JnettopEditSshConnectionControl extends JnettopEditConnectionControl {

	private JnettopSshConnectionFactory factory;

	private Text hostNameText;
	private Text portText;
	private Text userNameText;
	private Text commandText;
	private Text identityText;
	private Button chooseIdentityButton;
	
	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public JnettopEditSshConnectionControl(Composite parent, int style, JnettopSshConnectionFactory factory) {
		super(parent, style);
		this.factory = factory;
		createControls();
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	private void createControls() {
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 4;
		this.setLayout(gridLayout);
		
		GridData gd;

		final Label hostNameLabel = new Label(this, SWT.NONE);
		hostNameLabel.setText("Host name:");

		hostNameText = new Text(this, SWT.BORDER);
		gd = new GridData(GridData.FILL, GridData.CENTER, true, false);
		gd.widthHint = 314;
		hostNameText.setLayoutData(gd);

		final Label portLabel = new Label(this, SWT.NONE);
		portLabel.setText("Port:");

		portText = new Text(this, SWT.BORDER);
		gd = new GridData(GridData.FILL, GridData.CENTER, true, false);
		gd.widthHint = 76;
		portText.setLayoutData(gd);
		portText.setText("22");

		final Label userNameLabel = new Label(this, SWT.NONE);
		userNameLabel.setText("User name:");

		userNameText = new Text(this, SWT.BORDER);
		gd = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
		gd.widthHint = 128;
		gd.horizontalSpan = 3;
		userNameText.setLayoutData(gd);
		userNameText.setText("root");
		
		final Label identityLabel = new Label(this, SWT.NONE);
		identityLabel.setText("Identity file:");
		
		identityText = new Text(this, SWT.BORDER);
		gd = new GridData(GridData.FILL, GridData.CENTER, true, false);
		gd.widthHint = 314;
		identityText.setLayoutData(gd);
		
		chooseIdentityButton = new Button(this, SWT.NONE);
		chooseIdentityButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false, 2, 1));
		chooseIdentityButton.setText("...");
		chooseIdentityButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN);
				fileDialog.setText("Select identity file...");
				fileDialog.setFileName(identityText.getText());
				if (identityText.getText().length() == 0) {
					File f = new File(System.getProperty("user.home") + File.separatorChar + ".ssh");
					if (f.isDirectory()) {
						fileDialog.setFilterPath(f.getAbsolutePath());
					}
				} else {
					File f = new File(identityText.getText());
					File parentFile = f.getParentFile();
					if (parentFile != null)
						fileDialog.setFilterPath(parentFile.getAbsolutePath());
					fileDialog.setFileName(f.getName());
				}
				String fileName = fileDialog.open();
				if (fileName != null)
					identityText.setText(fileName);
			}
		});

		final Label commandLabel = new Label(this, SWT.NONE);
		commandLabel.setText("Command:");

		commandText = new Text(this, SWT.BORDER);
		commandText.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		commandText.setText("/usr/bin/jnettop --display jnet");
	}
	
	@Override
	public String validateAndGetError() {
		if (hostNameText.getText().length() == 0) {
			return "You have to specify host name.";
		}
		if (portText.getText().length() == 0) {
			return "You have to specify port.";
		}
		try {
			int i = Integer.parseInt(portText.getText());
			if (i<1 || i>65535)
				return "Port number is not in valid range.";
		} catch (NumberFormatException e) {
			return "Port number is not valid.";
		}
		if (userNameText.getText().length() == 0) {
			return "You have to specify username.";
		}
		if (commandText.getText().length() == 0) {
			return "You have to specify command to run.";
		}
		return null;
	}
	
	public String getHostName() {
		return hostNameText.getText();
	}
	
	public String getUserName() {
		return userNameText.getText();
	}
	
	public String getCommand() {
		return commandText.getText();
	}
	
	public String getIdentity() {
		return identityText.getText();
	}
	
	public int getPort() {
		try {
			return Integer.parseInt(portText.getText());
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	@Override
	public String getIntro() {
		return "SSH connection to the host tunnels the communication with remote jnettop process.";
	}
	
	public JnettopConnectionData getConnectionData() {
		return new JnettopSshConnectionData(factory,
				getHostName(),
				getPort(),
				getUserName(),
				getCommand(),
				getIdentity());
	}
}
