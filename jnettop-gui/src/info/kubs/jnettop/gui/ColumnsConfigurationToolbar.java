/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/ColumnsConfigurationToolbar.java,v 1.4 2006-04-30 12:52:49 merunka Exp $
 *
 */

package info.kubs.jnettop.gui;


import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

public class ColumnsConfigurationToolbar extends Window {

	private Table configuredTable;
	
	private ListViewer invisibleColumnsListViewer;
	private ListViewer visibleColumnsListViewer;
	private List visibleColumnsList;
	private List invisibleColumnsList;
	private Button moveDownButton;
	private Button moveUpButton;
	private Button makeInvisibleButton;
	private Button makeVisibleButton;
	
	private Button restoreDefaultButton;
	private Button setAsDefaultButton;
	
	private String tableName;
	
	private HashMap<TableColumn, ColumnData> columnData = new HashMap<TableColumn, ColumnData>();
	
	class TableColumnsListLabelProvider extends LabelProvider {
		public String getText(Object element) {
			return configuredTable.getColumn((Integer)element).getText();
		}
		public Image getImage(Object element) {
			return null;
		}
	}

	class TableColumnsContentProvider implements IStructuredContentProvider {
		private boolean visible;
		public TableColumnsContentProvider(boolean visible) {
			this.visible = visible;
		}
		public Object[] getElements(Object inputElement) {
			ArrayList<Object> al = new ArrayList<Object>();
			int[] columnOrder = configuredTable.getColumnOrder();
			
			for (int i=0; i<columnOrder.length; i++) {
				TableColumn column = configuredTable.getColumn(columnOrder[i]);
				if (visible == column.getWidth() > 0) {
					al.add(columnOrder[i]);
				}
			}
			
			return al.toArray();
		}
		public void dispose() {
		}
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
	
	public static class ColumnData {
		public static final ColumnData DEFAULT_VALUE = new ColumnData(100);

		public int preferredWidth;
		
		public ColumnData(int preferredWidth) {
			this.preferredWidth = preferredWidth;
		}		
	}
	
	public void setColumnData(TableColumn column, ColumnData data) {
		columnData.put(column, data);
	}
	
	public ColumnData getColumnData(TableColumn column) {
		return columnData.get(column);
	}
	
	private ColumnData getColumnDataOrNullValue(TableColumn column) {
		ColumnData cd = columnData.get(column);
		if (cd == null)
			return ColumnData.DEFAULT_VALUE;
		return cd;
	}
	
	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public ColumnsConfigurationToolbar(Shell parentShell, Table configuredTable, String tableName) {
		super(parentShell);
		this.setShellStyle(SWT.BORDER | SWT.CLOSE | SWT.TOOL);
		this.configuredTable = configuredTable;
		this.tableName = tableName;
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	//@Override
	protected Control createContents(Composite parent) {
		Composite container = (Composite) super.createContents(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 4;
		container.setLayout(gridLayout);

		final Label invisibleColumnsLabel = new Label(container, SWT.NONE);
		invisibleColumnsLabel.setText("Invisible Columns:");
		new Label(container, SWT.NONE);

		final Label visibleColumnsLabel = new Label(container, SWT.NONE);
		visibleColumnsLabel.setText("Visible Columns:");
		new Label(container, SWT.NONE);

		invisibleColumnsListViewer = new ListViewer(container, SWT.V_SCROLL | SWT.BORDER);
		invisibleColumnsListViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(final DoubleClickEvent e) {
				setColumnVisible(getSelectedInvisibleColumnId(), true);
			}
		});
		invisibleColumnsListViewer.setContentProvider(new TableColumnsContentProvider(false));
		invisibleColumnsListViewer.setLabelProvider(new TableColumnsListLabelProvider());
		invisibleColumnsListViewer.addPostSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(final SelectionChangedEvent e) {
				refreshButtons();
			}
		});
		invisibleColumnsList = invisibleColumnsListViewer.getList();
		
		invisibleColumnsList.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		invisibleColumnsListViewer.setInput(new Object());

		final Composite moveButtonsComposite = new Composite(container, SWT.NONE);
		final GridData gridData = new GridData(GridData.BEGINNING, GridData.CENTER, false, true);
		moveButtonsComposite.setLayoutData(gridData);
		moveButtonsComposite.setLayout(new GridLayout());

		makeVisibleButton = new Button(moveButtonsComposite, SWT.NONE);
		makeVisibleButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				setColumnVisible(getSelectedInvisibleColumnId(), true);
			}
		});
		makeVisibleButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));
		makeVisibleButton.setText(">>");

		makeInvisibleButton = new Button(moveButtonsComposite, SWT.NONE);
		makeInvisibleButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				setColumnVisible(getSelectedVisibleColumnId(), false);
			}
		});
		makeInvisibleButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		makeInvisibleButton.setText("<<");

		visibleColumnsListViewer = new ListViewer(container, SWT.V_SCROLL | SWT.BORDER);
		visibleColumnsListViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(final DoubleClickEvent e) {
				setColumnVisible(getSelectedVisibleColumnId(), false);
			}
		});
		visibleColumnsListViewer.setContentProvider(new TableColumnsContentProvider(true));
		visibleColumnsListViewer.setLabelProvider(new TableColumnsListLabelProvider());
		visibleColumnsListViewer.addPostSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(final SelectionChangedEvent e) {
				refreshButtons();
			}
		});
		visibleColumnsList = visibleColumnsListViewer.getList();
		visibleColumnsList.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		visibleColumnsListViewer.setInput(new Object());

		final Composite upDownButtonsComposite = new Composite(container, SWT.NONE);
		upDownButtonsComposite.setLayout(new GridLayout());

		moveUpButton = new Button(upDownButtonsComposite, SWT.NONE);
		moveUpButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				int oldSelection = visibleColumnsList.getSelectionIndex();
				moveColumns(getSelectedVisibleColumnId(), (Integer) visibleColumnsListViewer.getElementAt(oldSelection - 1));
				visibleColumnsListViewer.getList().setSelection(oldSelection - 1);
				refreshButtons();
			}
		});
		moveUpButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));
		moveUpButton.setText("Up");

		moveDownButton = new Button(upDownButtonsComposite, SWT.NONE);
		moveDownButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent e) {
				int oldSelection = visibleColumnsList.getSelectionIndex();
				moveColumns(getSelectedVisibleColumnId(), (Integer) visibleColumnsListViewer.getElementAt(oldSelection + 1));
				visibleColumnsListViewer.getList().setSelection(oldSelection + 1);
				refreshButtons();
			}
		});
		moveDownButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, false, false));
		moveDownButton.setText("Down");

		final Composite defaultsButtonComposite = new Composite(container, SWT.NONE);
		defaultsButtonComposite.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 4, 1));
		defaultsButtonComposite.setLayout(new GridLayout(2, false));
		
		restoreDefaultButton = new Button(defaultsButtonComposite, SWT.NONE);
		restoreDefaultButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		restoreDefaultButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				restoreDefaults();
				refreshButtons();
			}
		});
		restoreDefaultButton.setText("Restore Default");
		
		setAsDefaultButton = new Button(defaultsButtonComposite, SWT.NONE);
		setAsDefaultButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		setAsDefaultButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				saveAsDefaults();
				refreshButtons();
			}
		});
		setAsDefaultButton.setText("Set as Default");
		
		refreshButtons();
		//
		return container;
	}
	
	private int getSelectedVisibleColumnId() {
		IStructuredSelection selection = (IStructuredSelection) visibleColumnsListViewer.getSelection();
		if (selection.size() == 0)
			return -1;
		return (Integer) selection.getFirstElement();
	}
	
	private int getSelectedInvisibleColumnId() {
		IStructuredSelection selection = (IStructuredSelection) invisibleColumnsListViewer.getSelection();
		if (selection.size() == 0)
			return -1;
		return (Integer) selection.getFirstElement();
	}
	
	private void moveColumns(int columnId1, int columnId2) {
		int[] columnOrder = configuredTable.getColumnOrder();
		int columnIndex1 = -1, columnIndex2 = -1;
		for (int i=0; i<columnOrder.length; i++) {
			if (columnOrder[i] == columnId1)
				columnIndex1 = i;
			if (columnOrder[i] == columnId2)
				columnIndex2 = i;
		}
		int x = columnOrder[columnIndex1];
		columnOrder[columnIndex1] = columnOrder[columnIndex2];
		columnOrder[columnIndex2] = x;
		configuredTable.setColumnOrder(columnOrder);
		refreshTables();
		refreshButtons();
	}
	
	private void refreshButtons() {
		makeVisibleButton.setEnabled(getSelectedInvisibleColumnId() != -1);
		makeInvisibleButton.setEnabled(getSelectedVisibleColumnId() != -1);
		moveUpButton.setEnabled(getSelectedVisibleColumnId() != -1 && getSelectedVisibleColumnId() != (Integer) visibleColumnsListViewer.getElementAt(0));
		moveDownButton.setEnabled(getSelectedVisibleColumnId() != -1 && getSelectedVisibleColumnId() != (Integer) visibleColumnsListViewer.getElementAt(visibleColumnsListViewer.getList().getItemCount() - 1));
	}
	
	private void setColumnVisible(int columnId, boolean visible) {
		TableColumn column = configuredTable.getColumn(columnId);
		column.setWidth(visible ? getColumnDataOrNullValue(column).preferredWidth : 0);
		refreshTables();
		refreshButtons();
	}
	
	private void refreshTables() {
		if (visibleColumnsListViewer != null)
			visibleColumnsListViewer.setInput(new Object());
		if (invisibleColumnsListViewer != null)
			invisibleColumnsListViewer.setInput(new Object());
	}
	
	private String serializeSettings() {
		StringBuilder bld = new StringBuilder();
		int[] columnOrder = configuredTable.getColumnOrder();
		for (int i=0; i<columnOrder.length; i++) {
			if (i>0) bld.append(':');
			bld.append(columnOrder[i]);
		}
		bld.append(';');
		for (int i=0; i<configuredTable.getColumnCount(); i++) {
			if (i>0) bld.append(':');
			bld.append(configuredTable.getColumn(i).getWidth());			
		}
		return bld.toString();
	}
	
	private boolean deserializeSettings(String columnConfiguration) {
		if (columnConfiguration == null || columnConfiguration.length() == 0)
			return false;
		
		try {
			String[] dataPieces = columnConfiguration.split(";");
			
			String[] columnOrderS = dataPieces[0].split(":");
			int[] columnOrder = new int[columnOrderS.length];
			for (int i=0; i<columnOrder.length; i++)
				columnOrder[i] = Integer.parseInt(columnOrderS[i]);
			configuredTable.setColumnOrder(columnOrder);
			
			String[] columnWidthS = dataPieces[1].split(":");
			for (int i=0; i<columnWidthS.length; i++)
				configuredTable.getColumn(i).setWidth(Integer.parseInt(columnWidthS[i]));
			
			return true;
		} catch (Exception e) {
		} finally {
			refreshTables();
		}
		
		return false;
	}
	
	public void restoreDefaults() {
		String columnConfiguration = JnettopGui.getInstance().getPreference("defaultColumnConfiguration."+tableName, null);
		deserializeSettings(columnConfiguration);
		refreshTables();
	}
	
	public void saveAsDefaults() {
		JnettopGui.getInstance().setPreference("defaultColumnConfiguration." + tableName, serializeSettings());
	}
	
	public void restoreSettings(String settings) {
		if (!deserializeSettings(settings))
			restoreDefaults();
	}
	
	public String saveSettings() {
		return serializeSettings();
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(400, 280);
	}
	
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Column Configuration");
	}

	@Override
	protected Layout getLayout() {
		return new FillLayout();
	}
}
