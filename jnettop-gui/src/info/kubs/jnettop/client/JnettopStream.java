/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/client/JnettopStream.java,v 1.4 2006-04-30 12:52:49 merunka Exp $
 *
 */

package info.kubs.jnettop.client;

public class JnettopStream {
	public long uid;
	public String localAddress;
	public String remoteAddress;
	public String protocol;
	public int localPort;
	public int remotePort;
	
	public long localBytes;
	public long remoteBytes;
	public long totalBytes;
	public long localPackets;
	public long remotePackets;
	public long totalPackets;
	
	public long localBps;
	public long remoteBps;
	public long totalBps;
	public long localPps;
	public long remotePps;
	public long totalPps;
	
	public String filterData;
	
	private JnettopSession session;
	public JnettopStream(JnettopSession session) {
		this.session = session;
	}
	
	@Override
	public boolean equals(Object arg0) {
		if (!(arg0 instanceof JnettopStream)) {
			return false;
		}
		JnettopStream otherStream = (JnettopStream) arg0;
		return uid == otherStream.uid;
	}
	
	@Override
	public int hashCode() {
		return (int)(uid & 0xffffffff) ^ (int)(uid >>> 32);
	}
	
	private String localName;
	public String getLocalName() {
		if (localName == null)
			localName = session.resolveAddress(localAddress);
		return localName == null ? localAddress : localName;
	}
	
	private String remoteName;
	public String getRemoteName() {
		if (remoteName == null)
			remoteName = session.resolveAddress(remoteAddress);
		return remoteName == null ? remoteAddress : remoteName;
	}
}
